const helloWorld = require('./hello-world').helloWorld();

console.log(helloWorld);

module.exports = {
    helloWorld: helloWorld,
};
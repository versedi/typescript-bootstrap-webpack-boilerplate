const path = require('path');

module.exports = {
    entry: "./resources/assets/js/index.ts",
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx",".ts", ".tsx"]
    },
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'public/assets/js/')
    }
};